var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

// Schemas

var Book = new Schema({
    author: { type: String, required: true },
    title: { type: String, required: true },
    year: { type: Number, required: false },
    description: { type: String, required: true },
    images: { title: String, url:String },
    // likes: { type: Number, required: true },
    modified: { type: Date, default: Date.now }
});

// validation
Book.path('title').validate(function (v) {
    return v.length > 0 && v.length < 70;
});

var BookModel = mongoose.model('Book', Book);

module.exports = BookModel;

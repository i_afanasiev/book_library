
var mongoose = require('mongoose')
    , ObjectID = require("mongodb").ObjectID
    , Book = require('../models/book');

mongoose.Promise = global.Promise;

module.exports = function(app) {

    app.post('/add', function(req, res) {
        // FenShyi style
        var newBook = new Book(req.body);
        newBook.save().then(function () {
            res.sendStatus(200);
        }).catch(function (err) {
            console.log(err);
            res.send(err);
        });
    });

    app.post('/update', function(req, res) {
        let body = req.body;
        let changedBook = Book.findOneAndUpdate({id:ObjectID(body.id)}, {$set:body}
        ).exec().catch(function (err) {
            console.log(err);
        });

        res.send(changedBook);

    });

    app.post('/delete', function(req, res) {
        // findoneandremove to the rescue
        var body = req.body;

        Book.findByIdAndRemove(ObjectID(body._id), function (err) {
            if (err) {
                console.log(err);
                res.sendStatus(302);
            }
            else {
                res.sendStatus(200);
            }
        });
    });

    app.get('/get', function(req, res) {

        Book.find({}, function (err, books) {

            if (err) {
                console.log(err);
            }

            else {
                return res.json(books);
            }
        });
    });
};
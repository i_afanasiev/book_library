import Book from "./components/book.js"
import Sidebar from "./components/sidebar.js"
import React, { Component, PropTypes } from 'react'
import GetData from "./components/utils.js"
import Utils from "./components/utils.js"

// Main App Class
//


var App = React.createClass({

    getInitialState: function() {

        return {
            book_view: 'book_tile',
            list: [],
            filter: {
                author: '',
                title: '',
                year:  ''
            }
        };
    },

    applyFilter: function (filter) {

        this.setState({filter:filter}, this.booksFilter);

    },

    addBook: function(book) {
        // var data = new FormData();
        book.images.title = book.title;
        let self = this,
            newList = self.state.list;

        Utils.SendBook(book)
            .then(function (res) {
                console.log(res);
                newList.push(book);
                self.setState({list:newList}, self.booksFilter);
            })
            .catch(function (err) {
                console.log("Error occurs while sending data" + err);
            });


    },

    removeBook: function(bookID) {
        let self = this;
        Utils.DeleteBook(bookID, self.state.list)
            .then(function (booksList) {
                console.log(booksList);
                self.setState({list:booksList}, self.booksFilter);
            })
            .catch();
    },


    booksFilter: function () {
        var self = this;
        let filteredBooks = self.state.list.map(function(book) {

            book.isVisible = (book.author.indexOf(self.state.filter.author) > -1
            && book.title.indexOf(self.state.filter.title) > -1
            && (self.state.filter.year == '' || book.year == self.state.filter.year));

            return book;

        });
      this.setState({list: filteredBooks})
    },

    changeView: function(view) {
        this.setState({book_view: view})
    },

    componentDidMount: function() {
        let self = this;

            Utils.GetData()
            .then(function (json) {
                self.setState({list: json});
            })
            .catch(function (err) {
                console.log("Error occurs while processing: " + err);
                return err;
            })
    },

    render: function() {

        var self = this;
        return (
            <div className="row">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <Sidebar booksFilter={self.applyFilter}
                                     changeView={self.changeView}
                                     addBook={self.addBook}
                            />
                        </div>
                        <div className="col-md-8">
                            {self.state.list.map((item, index)=>item.isVisible ? <Book key={index}
                                                                                       book_view={self.state.book_view}
                                                                                       data={item}
                                                                                       deleteHandler={self.removeBook}/> : item.isVisible
                            )}

                        </div>
                    </div>
                </div>
            </div>

        );
    }
});

export default  App;


// В отдельный компонент пушить колбеки функций

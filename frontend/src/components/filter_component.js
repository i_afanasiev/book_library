import React, { Component, PropTypes } from 'react'

var ComponentsFilter = React.createClass({

    getInitialState: function () {
        return {
            filter: {
                author: '',
                title: '',
                year: ''
            }
        }
    },

    submitHandler: function (e) {
        e.preventDefault();

        this.props.booksFilter(this.state.filter);

    },

    authorHandler: function (e) {

        this.setState({filter: Object.assign(this.state.filter, {author: e.target.value})});

    },

    titleHandler: function (e) {
        this.setState({filter: Object.assign(this.state.filter, {title: e.target.value})});
    },

    yearHandler: function (e) {
        this.setState({filter: Object.assign(this.state.filter, {year: e.target.value})})
    },

    render: function () {
        var self = this;
        return (

                <form onSubmit={self.submitHandler}>
                    <input
                        className='author_input'
                        placeholder='По автору'
                        value={self.state.filter.author}
                        onChange={self.authorHandler}
                    />
                    <input
                        className='title_input'
                        placeholder='По названию'
                        value={self.state.title}
                        onChange={self.titleHandler}
                    />
                    <input
                        type="number"
                        className='year_input'
                        placeholder='По году'
                        value={self.state.year}
                        onChange={self.yearHandler}
                    />
                    <div>
                        <button type="submit" className="search">Поиск</button>
                    </div>
                </form>
        )
    }
});

export default ComponentsFilter;
import React, { Component, PropTypes } from 'react'



var LikesButton = React.createClass({

    getInitialState: function () {
        return {
            likes: 0
        }
    },

    onLikesClick: function() {
        this.setState({likes: ++this.state.likes});
    },
    render: function() {

        return (
            <div>
                <button
                    className={'like_button'}
                    onClick={this.onLikesClick}>
                    Like <i className="fa fa-thumbs-o-up"></i> {this.state.likes}
                </button>
            </div>

        )
    }
});

export default LikesButton;

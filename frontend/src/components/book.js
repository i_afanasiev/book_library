import React, { Component, PropTypes } from 'react'
import LikesButton from "./like_button.js"
import CloseButton from "./delete_button.js"

var Book = React.createClass({

    getInitialState: function() {
        return {
            visible: false
        };
    },

    readmoreClick: function(e) {
        e.preventDefault();
        this.setState({visible: true});
    },

    clickHandler: function() {
        this.props.deleteHandler(this.props.data._id)

    },

    render: function() {
        let data = this.props.data,
            visible = this.state.visible,
            view_class = this.props.book_view,
            author = data.author,
            title = data.title,
            description = data.description,
            year = data.year,
            cover = data.images.url;
            // likes = data.likes;

        return (
                <div className={"col-md-4 " + view_class}> {/*var for orientation determining*/}
                    <div className="border">
                        <div className="book_author">Автор: {author}</div>
                        <div className="book_title">Название: {title}</div>
                        <div className="book_year">Год выпуска: {year}</div>
                        <div className="book_annotation">
                            <img className="book_cover" src={cover}/>
                            <span className={"caption " + (visible ? '': 'short')}>
                                Описание:<br/> {description}
                            </span>
                        </div>

                        <a href="#"
                           className={'book_readMore ' + (visible ? 'hide': '')}
                           onClick={this.readmoreClick}>Подробнее</a>
                        {/*<LikesButton likes={likes}/>*/}
                        <CloseButton ClickHandler={this.clickHandler}/>

                    </div>
                </div>

        );
    }
});

export default Book;
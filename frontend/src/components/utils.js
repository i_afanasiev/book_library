import React, { Component, PropTypes } from 'react'

const backendURL = 'http://localhost:19000';


function GetData() {

        return fetch(new Request(backendURL + '/get'))
            .then(function (res) {
                if (res.ok) {
                    return res.json()
                }
                else {throw Error()}
            })
            .then(function (json) {
                // Adding isVisible attribute to determine what to render
                if (json) {
                    json.map(book => book.isVisible = true);
                    return json
                }

            })
            .catch(function (err) {
                console.log("Error occurs while processing: " + err);
                return err;
            })

}
function SendBook(book) {

    let data = JSON.stringify( book ),
        myHeaders = new Headers({"Accept": "application/json",
                                 "Content-Type": "application/json"});

    return fetch(backendURL + '/add', {
                method: 'POST',
                headers: myHeaders,
                mode: 'cors',
                cache: 'default',
                body: data
                })
                .then(function(res){
                    if (res.ok) {
                        return res
                    }
                    else {console.log("Something goes wrong");
                        throw new Error()}
                })
                .catch(function (err) {
                    console.log("Error occurs while sending data " + err);
                });

}

function DeleteBook(book_id, booksList) {
    let myHeaders = new Headers({
        "Accept": "application/json",
        "Content-Type": "application/json"
    });

    return fetch(backendURL + '/delete', {
        method: 'POST',
        headers: myHeaders,
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify({_id: book_id})
    })
        .then(function (res) {
            if (res.ok)  {
                for (let i=0; i<booksList.length; i++) {

                    if (booksList[i]._id === book_id) {
                        booksList.splice(i,1);
                        break
                    }
                }
                return booksList}
            else {throw Error()}

        })
        .catch(function (err) {
            console.log("Error occurs while sending data" + err);
        });


}

export default {GetData, SendBook, DeleteBook};
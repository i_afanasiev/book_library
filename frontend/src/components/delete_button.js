import React, { Component, PropTypes } from 'react'



var CloseButton = React.createClass({

    render: function() {

        return (
            <div>
                <button
                    className={'close_button'}
                    onClick={this.props.ClickHandler}>
                    <i className="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>

        )
    }
});

export default CloseButton;

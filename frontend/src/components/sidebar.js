import React, { Component, PropTypes } from 'react'
import BookAdding from "./add_book"
import ComponentsFilter from "./filter_component"


var Sidebar = React.createClass({

    chooseTileView: function(e) {
        e.preventDefault();
        this.props.changeView('book_tile');
    },

    chooseRaleView: function (e) {
        e.preventDefault();
        this.props.changeView('book_row');
    },


    render: function () {
        // console.log(this.props);
        var self = this;
        return (
            <div className="sidebar">
                <button className="tile_view" onClick={self.chooseTileView}><i className="fa fa-th-large"></i></button>
                <button className="rale_view" onClick={self.chooseRaleView}><i className="fa fa-th-list"></i></button>
                <div className="input_section_title">Поиск:

                    <ComponentsFilter booksFilter={self.props.booksFilter}/>
                </div>
                <BookAdding addBook={self.props.addBook}/>
            </div>
        )
    }
});

export default Sidebar;
import React, { Component, PropTypes } from 'react'



var BookAdding = React.createClass({

    getInitialState: function () {

        return {author: '',
            title: '',
            year:0,
            description: '',
            images: {title: "", url: ""},
            likes: 0
        }
    },


    addBookHandler: function(e) {
        e.preventDefault();
        this.props.addBook(this.state);
    },

    authorHandler: function (e) {
        this.setState({author: e.target.value})
    },

    titleHandler: function (e) {
        this.setState({title: e.target.value})
    },

    yearHandler: function (e) {
        this.setState({year: e.target.value})
    },

    descriptionHandler: function (e) {
        this.setState({description: e.target.value})
    },

    coverHandler: function (e) {
        this.setState({images: {url: e.target.value}})
    },

    render: function() {
        return (
            <div>Добавить книгу
                <form onSubmit={this.addBookHandler}>
                    <input
                        required
                        className='author-input'
                        defaultValue=''
                        placeholder='Введите автора'
                        onChange={this.authorHandler}
                    />
                    <input
                        required
                        className='title-input'
                        defaultValue=''
                        placeholder='Введите название'
                        onChange={this.titleHandler}
                    />
                    <input
                        type="number"
                        className='year-input'
                        defaultValue=''
                        placeholder='Введите год выхода'
                        onChange={this.yearHandler}
                    />
                    <input
                        className='annotation-input'
                        defaultValue=''
                        placeholder='Введите описание'
                        onChange={this.descriptionHandler}
                    />
                    <input
                        className='cover-input'
                        defaultValue=''
                        placeholder='Укажите ссылку на обложку'
                        onChange={this.coverHandler}
                    />
                    <div>
                        <button type="submit">Добавить</button>
                    </div>
                </form>
            </div>
        );
    }

});

export default BookAdding;
import React from 'react'
import { render } from 'react-dom'
import App from './app'
import './assets/css/app.css'           // <-- импорт стилей


render(
    <App />,
    document.getElementById('root')
);